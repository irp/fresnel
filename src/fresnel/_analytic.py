# SPDX-License-Identifier: GPL-3.0-or-later

import numpy as np
from scipy.special import erfi
import functools


def _is_between(x, xmin, xmax, dtype=np.float64):
    return ((x >= xmin) & (x <= xmax)).astype(dtype)


class DiskAperture:
    r"""Diffraction from a disk aperture in arbitrary dimensions.

    Parameters
    ----------
    w : float
        Aperture diameter
    ndim : int, default=1
        Number of dimensions
    wavelength : float, default=1.0
        Wave length
    dtype : dtype, default=np.complex128
        Data type of the wave field

    See Also
    --------
    RectangularAperture
    """

    def __init__(self, w, ndim=1, wavelength=1.0, dtype=np.complex128):
        self.r_red = 0.5 * np.asarray(w, dtype=np.float64) / wavelength
        self.ndim = ndim
        self.wavelength = wavelength
        self.dtype = dtype

    def __call__(self, *x):
        r"""Wave field in the exit plane.

        Parameters
        ----------
        *x : array_like
            Coordinates. Pass one parameter for each dimension.

        Returns
        -------
        u0 : array_like
            Wave field in the exit plane.
        """
        if len(x) != self.ndim:
            raise ValueError(f"Dimension mismatch: {len(x)} != {self.ndim}")

        x2 = tuple(map(np.square, x))
        r2 = functools.reduce(np.add, x2) / np.square(self.wavelength)

        return (r2 <= np.square(self.r_red)).astype(self.dtype)


class RectangularAperture:
    r"""Diffraction from a rectangular aperture in arbitrary dimensions.

    Parameters
    ----------
    w : tuple of floats
        Diameters of the aperture in each dimension (w1, w2, ...)
    wavelength : float, default=1.0
        Wave length
    dtype : dtype, default=np.complex128
        Data type of the wave field

    See Also
    --------
    RectangularAperture
    """

    def __init__(self, w, wavelength=1.0, dtype=np.complex128):
        self.w_red = np.atleast_1d(np.asarray(w, dtype=np.float64)) / wavelength
        self.wavelength = wavelength
        self.dtype = dtype

    @property
    def ndim(self):
        r"""
        Number of dimensions
        """
        return self.w_red.size

    def __call__(self, *x):
        r"""Wave field in the exit plane.

        Parameters
        ----------
        *x : array_like
            Coordinates. Pass one parameter for each dimension.

        Returns
        -------
        u0 : array_like
            Wave field in the exit plane.
        """
        if len(x) != self.ndim:
            raise ValueError(f"Dimension mismatch: {len(x)} != {self.ndim}")

        def _f(xi, wi):
            return _is_between(xi / self.wavelength, -wi / 2, wi / 2, dtype=self.dtype)

        profiles = tuple(map(_f, x, self.w_red))

        return functools.reduce(np.multiply, profiles)

    @staticmethod
    def _near_field_1d(u, w_red, z_red):
        def _f(x, z):
            return erfi(np.sqrt(np.pi * 1j) / np.sqrt(z) * x)

        return 0.5j * (_f(-0.5 * w_red + u, z_red) - _f(0.5 * w_red + u, z_red))

    @staticmethod
    def _far_field_1d(theta, w_red, z_red, with_phase=True):
        if with_phase:
            phase = np.exp(1j * np.pi * z_red * theta**2) / np.sqrt(1j)
        else:
            phase = 1

        # np.sinc(x) = np.sin(np.pi * x) / (np.pi * x)
        return phase * w_red / np.sqrt(z_red) * np.sinc(w_red * theta)

    def near_field(self, *x, dz=1):
        r"""Wave field in the near field.

        Parameters
        ----------
        *x : array_like
            Coordinates. Pass one parameter for each dimension.
        dz : float, default=1.0
            Propagation distance.

        Returns
        -------
        u0 : array_like
            Wave field in the near field.
        """
        z_red = dz / self.wavelength

        # compute _near_field_1d for each dimension separately
        def _f(xi, wi):
            return self._near_field_1d(xi / self.wavelength, wi, z_red)

        profiles = tuple(map(_f, x, self.w_red))

        return functools.reduce(np.multiply, profiles)

    def far_field(self, *theta, dz=1, with_phase=True):
        r"""Wave field in the far field.

        Parameters
        ----------
        *x : array_like
            Coordinates. Pass one parameter for each dimension.
        dz : float, default=1.0
            Propagation distance.
        with_phase : bool, default=True
            If set to false, the phase chirp is not computed.

        Returns
        -------
        u0 : array_like
            Wave field in the far field.

        Notes
        -----
        The distance ``dz`` is required to correctly compute the scaling and phase factor.
        """
        z_red = dz / self.wavelength

        # compute _far_field_1d for each dimension separately
        def _f(thi, wi):
            return self._far_field_1d(thi, wi, z_red, with_phase)

        profiles = tuple(map(_f, theta, self.w_red))
        return functools.reduce(np.multiply, profiles)


class GaussianAperture:
    r"""Diffraction from a Gaussian aperture in arbitrary dimensions.

    Parameters
    ----------
    w_0 : tuple of floats
        Beam waist for each dimension (w1, w2, ...)
    wavelength : float, default=1.0
        Wave length
    dtype : dtype, default=np.complex128
        Data type of the wave field

    See Also
    --------
    RectangularAperture
    """

    def __init__(self, w0, wavelength=1.0):
        self.w0_red = np.atleast_1d(np.asarray(w0, dtype=np.float64)) / wavelength
        self.wavelength = wavelength
        self.zR_red = np.pi * self.w0_red**2

    @property
    def ndim(self):
        r"""
        Number of dimensions
        """
        return self.w0_red.size

    @staticmethod
    def _near_field_1d(u, zR_red, z_red):
        iq = 1j * z_red + zR_red
        return np.sqrt(zR_red) / np.sqrt(iq) * np.exp(-np.pi * u**2 / iq)

    @staticmethod
    def _far_field_1d(th, zR_red, z_red):
        bariq = -1j * z_red + zR_red  # complex conjugate of iq
        return np.sqrt(zR_red) / np.sqrt(1j * z_red) * np.exp(-np.pi * bariq * th**2)

    def near_field(self, *x, dz=1):
        r"""Wave field in the near field.

        Parameters
        ----------
        *x : array_like
            Coordinates. Pass one parameter for each dimension.
        dz : float, default=1.0
            Propagation distance.

        Returns
        -------
        u0 : array_like
            Wave field in the near field.
        """
        z_red = dz / self.wavelength

        # compute _near_field_1d for each dimension separately
        def _f(xi, zRi):
            return self._near_field_1d(xi / self.wavelength, zRi, z_red)

        profiles = tuple(map(_f, x, self.zR_red))
        return functools.reduce(np.multiply, profiles)

    def far_field(self, *theta, dz=1):
        r"""Wave field in the far field.

        Parameters
        ----------
        *x : array_like
            Coordinates. Pass one parameter for each dimension.
        dz : float, default=1.0
            Propagation distance.

        Returns
        -------
        u0 : array_like
            Wave field in the far field.

        Notes
        -----
        The distance ``dz`` is required to correctly compute the scaling and phase factor.
        """
        z_red = dz / self.wavelength

        # compute _far_field_1d for each dimension separately
        def _f(thi, zRi):
            return self._far_field_1d(thi, zRi, z_red)

        profiles = tuple(map(_f, theta, self.zR_red))
        return functools.reduce(np.multiply, profiles)

    def __call__(self, *x, z=0):
        r"""Shorthand for ``near_field`` with ``z=0``."""
        return self.near_field(*x, dz=z)


def gaussian_beam1d(w_0, r, z=0, wavelength=1):
    r"""A 1+1 dimensional Gaussian beam.

    Parameters
    ----------
    w_0 : float
        Beam waist
    r : array_like
        Radial coordinate.
    dz : float, default=1.0
        Propagation distance.
    wavelength : float, default=1.0
        Wavelength of the beam.

    Returns
    -------
    u0 : array_like
        Gaussian beam solution.

    Notes
    -----
    The coordinates ``r`` and ``z`` are broadcasted so the 2d field can be computed in one call.
    """
    f = GaussianAperture(w_0, wavelength)
    return f(r, z=z)
