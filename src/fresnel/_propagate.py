# SPDX-License-Identifier: GPL-3.0-or-later

import numpy as np

from ._vacuum import ASMPropagator
from ._finite_differences import Solver2D, Solver2DSym, Solver3D

_lambda0 = 1.0  # all lengths are in units of the wavelength
_k0 = 2 * np.pi / _lambda0


def _compute_potential(n_, k_=_k0):
    return -1j * k_ / 2 * (1 - n_ * n_)


class MultislicePropagator:
    r"""Multi-slice propagation in arbitrary dimensions.

    Propagate a wave field using the Multi-slice method (see :footcite:p:`Paganin__2006` or :footcite:p:`Li_OE_2017`).

    Parameters
    ----------
    u0 : array-like
        Initial wave field.
    d : tuple
        Step sizes `(dz, dy, dx)` for 3 dimnsions or `(dz, dx)` for 2 dimensions.
    wl : float, default=1.0
        Wavelength of the wave field.

    References
    ----------
    .. footbibliography::
    """

    _dtype = np.complex128

    def __init__(self, u0, d, wl=1.0):
        d = tuple(d)
        _ndim = len(d) - 1
        u0 = np.asarray(u0, dtype=self._dtype)

        if u0.ndim != _ndim:
            raise ValueError("u0.ndim must be equal to len(d) - 1")

        self._shape = u0.shape
        self._dz = d[0]
        self._dperp = np.asarray(d[1:])
        self._k = _k0 / wl

        # ASM Propagator without padding
        self._fs_propagator = ASMPropagator(self._shape, self._dperp, self._dz, wl, npad=1)

        self.u = u0

    def step(self, n, workers=-1):
        r"""Compute one step of the propagation.

        Propagates the wavefield from `z` to `z + dz`.

        Parameters
        ----------
        n : array-like
            Refractive index profile at the current depth.
        workers : int, default=-1
            Number of worker threads to use for the FFT.

        Returns
        -------
        u : array-like
            Propagated wave field
        """
        n = np.asarray(np.broadcast_to(n, self._shape), dtype=self._dtype)
        F = _compute_potential(n, self._k)

        real_kernel = np.exp(self._dz * F)
        up = real_kernel * self.u
        self.u = self._fs_propagator(up, workers)

        return self.u


class FDPropagator2d:
    r"""Finite-difference propagation in 2 dimensions.

    Propagate a wave field using the finite-difference method (see :footcite:p:`Fuhse_PBCM_2005` or
    :footcite:p:`Soltau_OE_2021`).

    Parameters
    ----------
    n0 : array-like, 1-dimensional
        Initial refractive index profile.
    u0 : array-like, 1-dimensional
        Initial wave field.
    dz : float
        Step size in the propagation direction.
    dx : float
        Step size in the transversal direction.
    pml_width: float, default=0
        With of the perfectly matched layer (PML).
    sigma_max: float, default=0.1
        Absorption parameter of the perfectly matched layer (PML).
    wl : float, default=1.0
        Wavelength of the wave field.

    References
    ----------
    .. footbibliography::
    """

    _dtype = np.complex128

    def __init__(self, n0, u0, dz, dx, pml_width=0, sigma_max=0.1, wl=1.0):
        self._k = _k0 / wl
        A = 1j / (2 * self._k)
        sigma_max_rel = sigma_max * wl

        _ndim = 1
        u0 = np.ascontiguousarray(u0, dtype=self._dtype)
        if u0.ndim != _ndim:
            raise ValueError(f"u0.ndim must be equal to {_ndim}")

        nx = u0.shape[-1]
        self.solver = Solver2D(nx, A, dz, dx, pml_width, sigma_max_rel)

        self._shape = (nx,)

        n0 = np.ascontiguousarray(np.broadcast_to(n0, self._shape), dtype=self._dtype)

        F0 = _compute_potential(n0, self._k)

        self.solver.initialize(F0, u0)

    def step(self, n, boundary):
        r"""Compute one step of the propagation.

        Propagates the wavefield from `z` to `z + dz`.

        Parameters
        ----------
        n : array-like
            Refractive index profile at the current depth.
        boundary : (u1, uN)
            Boundary values of the wave field at the current depth.

        Returns
        -------
        u : array-like
            Propagated wave field
        """
        n = np.ascontiguousarray(np.broadcast_to(n, self._shape), dtype=self._dtype)
        F = _compute_potential(n, self._k)
        self.solver.step(F, *boundary)

        return self.solver.u


class FDPropagatorCS:
    r"""Finite-difference propagation with cylindrical (axial) symmetry.

    Propagate an axial symmetric wave field using the finite-difference method (see :footcite:p:`Soltau_OE_2021`).

    Parameters
    ----------
    n0 : array-like, 1-dimensional
        Initial refractive index profile.
    u0 : array-like, 1-dimensional
        Initial wave field.
    dz : float
        Step size in the propagation direction.
    dx : float
        Step size in the transversal direction.
    pml_width: float, default=0
        With of the perfectly matched layer (PML).
    sigma_max: float, default=0.1
        Absorption parameter of the perfectly matched layer (PML).
    wl : float, default=1.0
        Wavelength of the wave field.

    References
    ----------
    .. footbibliography::
    """

    _dtype = np.complex128

    def __init__(self, n0, u0, dz, dx, pml_width=0, sigma_max=0.1, wl=1.0):
        self._k = _k0 / wl
        A = 1j / (2 * self._k)
        sigma_max_rel = sigma_max * wl

        _ndim = 1
        u0 = np.ascontiguousarray(u0, dtype=self._dtype)
        if u0.ndim != _ndim:
            raise ValueError(f"u0.ndim must be equal to {_ndim}")
        nx = u0.shape[-1]

        self.solver = Solver2DSym(nx, A, dz, dx, pml_width, sigma_max_rel)

        self._shape = (nx,)
        n0 = np.ascontiguousarray(np.broadcast_to(n0, self._shape), dtype=self._dtype)
        F0 = _compute_potential(n0, self._k)

        self.solver.initialize(F0, u0)

    def step(self, n, boundary):
        r"""Compute one step of the propagation.

        Propagates the wavefield from `z` to `z + dz`.

        Parameters
        ----------
        n : array-like
            Refractive index profile at the current depth.
        boundary : (uN,)
            Boundary value of the wave field at the maximum radius at the current depth.

        Returns
        -------
        u : array-like
            Propagated wave field
        """
        n = np.ascontiguousarray(np.broadcast_to(n, self._shape), dtype=self._dtype)
        F = _compute_potential(n, self._k)
        self.solver.step(F, *boundary)

        return self.solver.u


class FDPropagator3d:
    r"""Finite-difference propagation in 3 dimensions.

    Propagate a wave field using the finite-difference method (see :footcite:p:`Fuhse_AO_2006` and
    :footcite:p:`Soltau_OE_2021`).

    Parameters
    ----------
    n0 : array-like, 1-dimensional
        Initial refractive index profile.
    u0 : array-like, 1-dimensional
        Initial wave field.
    dz : float
        Step size in the propagation direction.
    dy : float
        Step size in the 2nd transversal direction.
    dx : float
        Step size in the 1st transversal direction.
    pml_width: float, default=0
        With of the perfectly matched layer (PML).
    sigma_max: float, default=0.1
        Absorption parameter of the perfectly matched layer (PML).
    wl : float, default=1.0
        Wavelength of the wave field.

    References
    ----------
    .. footbibliography::
    """

    _dtype = np.complex128

    def __init__(self, n0, u0, dz, dy, dx, wl=1.0):
        self._k = _k0 / wl
        A = 1j / (2 * self._k)

        _ndim = 2
        u0 = np.ascontiguousarray(u0, dtype=self._dtype)
        if u0.ndim != _ndim:
            raise ValueError(f"u0.ndim must be equal to {_ndim}")
        nx = u0.shape[-1]
        ny = u0.shape[-2]

        self.solver = Solver3D(ny, nx, A, dz, dy, dx)

        self._shape = (ny, nx)
        self._ones_bx = np.ones((nx,), dtype=self._dtype)
        self._ones_by = np.ones((ny,), dtype=self._dtype)

        n0 = np.ascontiguousarray(np.broadcast_to(n0, self._shape), dtype=self._dtype)
        F0 = _compute_potential(n0, self._k)

        self.solver.initialize(F0.T, u0.T)

    def step(self, n, boundary):
        r"""Compute one step of the propagation.

        Propagates the wavefield from `z` to `z + dz`.

        Parameters
        ----------
        n : array-like
            Refractive index profile at the current depth.
        boundary : (uxl, uxr, uyl, uyr)
            Boundary values of the wave field at the current depth.

        Returns
        -------
        u : array-like
            Propagated wave field
        """
        n = np.ascontiguousarray(np.broadcast_to(n, self._shape), dtype=self._dtype)
        F = _compute_potential(n, self._k)

        bxl = boundary[0] * self._ones_by
        bxr = boundary[1] * self._ones_by
        byl = boundary[2] * self._ones_bx
        byr = boundary[3] * self._ones_bx

        self.solver.step(
            F.T,
            bxl,
            bxr,
            byl,
            byr,
        )
        u_T = self.solver.u

        return u_T.T
