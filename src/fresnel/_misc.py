# SPDX-License-Identifier: GPL-3.0-or-later

import numpy as np
from scipy.fft import fftfreq
from functools import reduce


def fftfreqn(n, dx=1):
    """
    Return the nd Discrete Fourier Transform sample frequencies.

    Parameters
    ----------
    n : int or tuple of ints
        Window lengths.
    dx: float or tuple floats, optional (Default = 1.0)
        Sample spacings.

    Returns
    -------
    xi1, xi2, ..., xin : ndarray

    See Also
    --------
    scipy.fft.fftfreq
    """
    n = np.asarray(n)
    ndim = n.size

    dx = np.ones(ndim, dtype=float) * np.asarray(dx)

    xi = [fftfreq(n_i, dx_i) for n_i, dx_i in zip(n, dx)]

    return np.meshgrid(*xi, sparse=True, indexing="ij")


def gridn(n):
    n = np.asarray(n)
    ndim = n.size

    # magic from fftfreq
    x = [
        np.concatenate([np.arange(0, (n[dim] - 1) // 2 + 1), np.arange(-(n[dim] // 2), 0)])
        for dim in range(ndim)
    ]

    return np.meshgrid(*x, sparse=True, indexing="ij")


def crop(a, crop_width):
    slices = [slice(w[0], -w[1] if w[1] > 0 else None) for w in crop_width]

    return a[tuple(slices)]


def squaresum(a):
    return reduce(np.add, map(np.square, a))
