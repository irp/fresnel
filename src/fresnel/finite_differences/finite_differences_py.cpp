#include <nanobind/nanobind.h>
#include <nanobind/eigen/dense.h>
#include <nanobind/stl/complex.h>

#include "./finite_differences.h"

namespace nb = nanobind;
namespace fd = finite_differences;

NB_MODULE(_finite_differences, m) {
  nb::class_<fd::Solver2D>(m, "Solver2D")
      .def(nb::init<const fd::index, const fd::complex, const fd::real,
                    const fd::real, const size_t, const fd::real>())
      .def("initialize", &fd::Solver2D::initialize, nb::arg("F0").noconvert(),
           nb::arg("u0").noconvert())
      .def("step", &fd::Solver2D::step, nb::arg("F").noconvert(),
           nb::arg("bl").noconvert(), nb::arg("br").noconvert())
      .def_rw("u", &fd::Solver2D::u);

  nb::class_<fd::Solver2DSym>(m, "Solver2DSym")
      .def(nb::init<const fd::index, const fd::complex, const fd::real,
                    const fd::real, const size_t, const fd::real>())
      .def("initialize", &fd::Solver2DSym::initialize,
           nb::arg("F0").noconvert(), nb::arg("u0").noconvert())
      .def("step", &fd::Solver2DSym::step, nb::arg("F").noconvert(),
           nb::arg("br").noconvert())
      .def_rw("u", &fd::Solver2DSym::u);

  nb::class_<fd::Solver3D>(m, "Solver3D")
      .def(nb::init<const fd::index, const fd::index, const fd::complex,
                    const fd::real, const fd::real, const fd::real>())
      .def("initialize", &fd::Solver3D::initialize, nb::arg("F0").noconvert(),
           nb::arg("u0").noconvert())
      .def("step", &fd::Solver3D::step, nb::arg("F").noconvert(),
           nb::arg("bxl").noconvert(), nb::arg("bxr").noconvert(),
           nb::arg("byl").noconvert(), nb::arg("byr").noconvert())
      .def_rw("u", &fd::Solver3D::u);
}
