#pragma once

#include <Eigen/Dense>
#include <complex>
#include <utility>  // std::move

namespace algebra {

template <typename T>
using array1d = Eigen::Array<T, Eigen::Dynamic, 1>;

template <typename T>
using TriMatrix = Eigen::Array<T, 3, Eigen::Dynamic>;

/* Solve the tridiagonal matrix equation with Thomas algorithm.
 * a_i x_{i-1} + b_i x_i  + c_i x_{i+1} = r_i
 *
 * Returns x
 *
 * Modifies the input coefficients b and r!
 */

template <typename T>
array1d<T> tridiagonal(TriMatrix<T> &&m, array1d<T> &&r) {
  int n = r.size();

  auto a = m.row(0);
  auto b = m.row(1);
  auto c = m.row(2);

  array1d<T> x(n, 1);

  for (int i = 1; i < n; i++) {
    T w = a(i) / b(i - 1);
    b(i) -= w * c(i - 1);
    r(i) -= w * r(i - 1);
  }

  x(n - 1) = r(n - 1) / b(n - 1);

  for (int i = n - 2; i >= 0; i--) {
    x(i) = (r(i) - c(i) * x(i + 1)) / b(i);
  }

  return x;
}
}  // namespace algebra

namespace finite_differences {

using index = Eigen::Index;
using real = double;
using complex = std::complex<real>;
using scalar = complex;
using array_1D = Eigen::Array<scalar, Eigen::Dynamic, 1>;
using array_2D = Eigen::Array<scalar, Eigen::Dynamic, Eigen::Dynamic>;

const array_1D compute_sigma(const size_t width, const size_t pml_width) {
  array_1D sigma = array_1D::Zero(width);

  if (pml_width > 0) {
    const array_1D ramp =
        array_1D::LinSpaced(pml_width + 1, 0, 1).tail(pml_width);

    sigma.head(pml_width) = ramp.square().reverse();
    sigma.tail(pml_width) = ramp.square();
  }

  return sigma;
}

const array_1D compute_eta(const size_t width, const size_t pml_width,
                           const real sigma_max) {
  const complex i(0, 1);
  const array_1D sigma = compute_sigma(width, pml_width);
  const array_1D eta = 1.0 / (1.0 + i * sigma_max * sigma);
  return eta;
}

class Solver2D {
 public:
  const index nx;
  const real dz;
  const real dx;

  const complex a;
  const array_1D eta;

  array_1D f;
  array_1D u;

 private:
  array_1D fp;

 public:
  Solver2D(const index nx, const complex A, const real dz, const real dx,
           const size_t pml_width, const real sigma_max)
      : nx{nx},
        dz{dz},
        dx{dx},
        a{compute_a(A, dz, dx)},
        eta{compute_eta(nx, pml_width, sigma_max)},
        f{nx},
        u{nx},
        fp{nx} {}

  void initialize(const Eigen::Ref<const array_1D> &F0,
                  const Eigen::Ref<const array_1D> &u0) {
    f = compute_f(F0, dz);
    u = u0;
  }

  void step(const Eigen::Ref<const array_1D> &F, const scalar bl,
            const scalar br) {
    fp = std::move(f);
    f = compute_f(F, dz);

    // _p :  i
    // _  :  i+1

    const index n = nx - 2;
    // assert eta.size == n + 2

    // setup tridiagonal n x n matrix M = tridiag(a,b,c) and rhs r

    algebra::TriMatrix<scalar> m(3, n);
    array_1D r(n, 1);

    // Note that matrix indices go from 1 to up.size()-1, so that indices are
    // off by 1

    for (index i = 1; i <= n; ++i) {
      m(0, i - 1) = -a * eta(i) *
                    (0.25 * eta(i - 1) + eta(i) - 0.25 * eta(i + 1));  // lower
      m(1, i - 1) = 2.0 + 2.0 * a * eta(i) * eta(i) - f(i);  // diagonal
      m(2, i - 1) =
          -a * eta(i) *
          (-0.25 * eta(i - 1) + eta(i) + 0.25 * eta(i + 1));  // upper;
      r(i - 1) = a * eta(i) * (0.25 * eta(i - 1) + eta(i) - 0.25 * eta(i + 1)) *
                     u(i - 1) +
                 (2.0 - 2.0 * a * eta(i) * eta(i) + fp(i)) * u(i) +
                 a * eta(i) *
                     (-0.25 * eta(i - 1) + eta(i) + 0.25 * eta(i + 1)) *
                     u(i + 1);
    }

    m(0, 0) = 0;
    m(2, n - 1) = 0;

    // boundary
    r(0) += a * bl * eta(1) * (0.25 * eta(0) + eta(1) - 0.25 * eta(2));
    r(n - 1) +=
        a * br * eta(n) * (-0.25 * eta(n - 1) + eta(n) + 0.25 * eta(n + 1));

    // insert boundary values
    u(0) = bl;
    u(n + 1) = br;

    u.segment(1, n) = algebra::tridiagonal(std::move(m), std::move(r));
  }

 private:
  static const complex compute_a(const complex A, const real dz,
                                 const real dx) {
    return A * dz / (dx * dx);
  }

  static const array_1D compute_f(const Eigen::Ref<const array_1D> &F,
                                  const real dz) {
    return F * dz;
  }
};

class Solver2DSym {
 public:
  const index nx;
  const real dz;
  const real dx;

  const complex a;
  const array_1D eta;

  array_1D f;
  array_1D u;

 private:
  array_1D fp;

 public:
  Solver2DSym(const index nx, const complex A, const real dz, const real dx,
              const size_t pml_width, const real sigma_max)
      : nx{nx},
        dz{dz},
        dx{dx},
        a{compute_a(A, dz, dx)},
        eta{compute_eta(nx, pml_width, sigma_max)},
        f{nx},
        u{nx},
        fp{nx} {}

  void initialize(const Eigen::Ref<const array_1D> &F0,
                  const Eigen::Ref<const array_1D> &u0) {
    f = compute_f(F0, dz);
    u = u0;
  }

  void step(const Eigen::Ref<const array_1D> &F, const scalar br) {
    fp = std::move(f);
    f = compute_f(F, dz);

    const index n = nx - 1;

    // setup tridiagonal n x n matrix M = tridiag(a,b,c) and rhs r

    algebra::TriMatrix<scalar> m(3, n);
    array_1D r(n, 1);

    for (index i = 1; i < n; ++i) {
      complex rhix = 1.0 / static_cast<complex>(i);

      m(0, i) = -(1.0 - 0.5 * rhix) * a;  // lower
      m(1, i) = 2.0 + 2.0 * a - f(i);     // diagonal
      m(2, i) = -(1.0 + 0.5 * rhix) * a;  // upper;
      r(i) = (1.0 - 0.5 * rhix) * a * u(i - 1) +
             (2.0 - 2.0 * a + fp(i)) * u(i) + (1.0 + 0.5 * rhix) * a * u(i + 1);
    }

    // left boundary
    m(0, 0) = 0;                     // lower
    m(1, 0) = 2.0 + 4.0 * a - f(0);  // diagonal
    m(2, 0) = -4.0 * a;              // upper;
    r(0) = (2.0 - 4.0 * a + f(0)) * u(0) + 4.0 * a * u(1);

    // right boundary
    complex rxnh = 1.0 / static_cast<complex>(n - 1);
    r(n - 1) += (1.0 + 0.5 * rxnh) * a * br;
    m(2, n - 1) = 0;

    u(n) = br;
    u.segment(0, n) = algebra::tridiagonal(std::move(m), std::move(r));
  }

 private:
  static const complex compute_a(const complex A, const real dz,
                                 const real dx) {
    return A * dz / (dx * dx);
  }

  static const array_1D compute_f(const Eigen::Ref<const array_1D> &F,
                                  const real dz) {
    return F * dz;
  }
};

/* Two-step alternating-direction Peaceman-Rachford scheme
   (J. W. Thomas: Numerical Partial Differential Equations)
*/

class Solver3D {
 public:
  const index ny;
  const index nx;
  const real dz;
  const real dy;
  const real dx;

  const complex ay;
  const complex ax;

  array_2D f;
  array_2D u;

  // cache
 private:
  array_2D fp;
  array_2D uhalf;
  array_2D u_trans;

 public:
  Solver3D(const index ny, const index nx, const complex A, const real dz,
           const real dy, const real dx)
      : ny{ny},
        nx{nx},
        dz{dz},
        dy{dy},
        dx{dx},
        ay{compute_a(A, dz, dy)},
        ax{compute_a(A, dz, dx)},
        f{nx, ny},
        u{nx, ny},
        fp{nx, ny},
        uhalf{nx, ny},
        u_trans{ny, nx} {}

  void initialize(const Eigen::Ref<const array_2D> &F0,
                  const Eigen::Ref<const array_2D> &u0) {
    if (F0.rows() != nx || F0.cols() != ny) {
      throw std::runtime_error("F0 shape mismatch");
    }

    if (u0.rows() != nx || u0.cols() != ny) {
      throw std::runtime_error("u0 shape mismatch");
    }

    f = compute_f(F0, dz);
    u = u0;
  }

  void step(const Eigen::Ref<const array_2D> &F,
            const Eigen::Ref<const array_1D> &bxl,
            const Eigen::Ref<const array_1D> &bxr,
            const Eigen::Ref<const array_1D> &byl,
            const Eigen::Ref<const array_1D> &byr) {
    if (F.rows() != nx || F.cols() != ny) {
      throw std::runtime_error("F shape mismatch");
    }

    fp = std::move(f);
    f = compute_f(F, dz);

    // .p :  i
    // .  :  i+1

    // coordinate system:
    // numpy (z,y,x), eigen (rows, cols): x <-> cols, y <-> rows

    const index nx_in = nx - 2;
    const index ny_in = ny - 2;

    // first halfstep

    // interpolate boundary values
    // WARNING: from my understand this should work without the tranpose(). it
    // compiles fine but produces NaNs...
    array_1D bxl_half = 0.5 * bxl + 0.5 * u.row(0).transpose();
    array_1D bxr_half = 0.5 * bxr + 0.5 * u.row(nx_in + 1).transpose();
    array_1D byl_half = 0.5 * byl + 0.5 * u.col(0);
    array_1D byr_half = 0.5 * byr + 0.5 * u.col(ny_in + 1);

    // fill in boundary values. x is done in the loop
    uhalf.col(0) = byl_half;
    uhalf.col(ny_in + 1) = byr_half;

#pragma omp parallel for schedule(static, 128)
    for (index iy = 1; iy <= ny_in; ++iy) {
      algebra::TriMatrix<scalar> m(3, nx_in);
      array_1D r(nx_in, 1);

      for (index ix = 1; ix <= nx_in; ++ix) {
        m(0, ix - 1) = -ax;
        m(1, ix - 1) = 2.0 + 2.0 * ax - 0.5 * f(ix, iy);
        m(2, ix - 1) = -ax;

        r(ix - 1) = ay * u(ix, iy - 1) +
                    (2.0 - 2.0 * ay + 0.5 * fp(ix, iy)) * u(ix, iy) +
                    ay * u(ix, iy + 1);
      }

      // boundary conditions - interpolate for half-step
      r(0) += ax * bxl_half(iy);
      r(nx_in - 1) += ax * bxr_half(iy);

      uhalf(0, iy) = bxr_half(iy);
      uhalf(nx_in + 1, iy) = bxl_half(iy);

      uhalf.col(iy).segment(1, nx_in) =
          algebra::tridiagonal(std::move(m), std::move(r));
    }

    // second halfstep

    // note: internal storage is column-major such that column-wise access would
    // be much faster

    // populate boundary values. y is done in the loop
    u_trans.col(0) = bxl;
    u_trans.col(nx_in + 1) = bxr;

#pragma omp parallel for schedule(static, 128)
    for (index ix = 1; ix <= nx_in; ++ix) {
      algebra::TriMatrix<scalar> m(3, ny_in);
      array_1D r(ny_in, 1);

      for (index iy = 1; iy <= ny_in; ++iy) {
        m(0, iy - 1) = -ay;
        m(1, iy - 1) = 2.0 + 2.0 * ay - 0.5 * f(ix, iy);
        m(2, iy - 1) = -ay;

        r(iy - 1) = ax * uhalf(ix - 1, iy) +
                    (2.0 - 2.0 * ax + 0.5 * fp(ix, iy)) * uhalf(ix, iy) +
                    ax * uhalf(ix + 1, iy);
      }

      // boundary conditions
      r(0) += ay * byl(ix);
      r(ny_in - 1) += ay * byr(ix);

      u_trans(0, ix) = byl(ix);
      u_trans(ny_in + 1, ix) = byr(ix);

      u_trans.col(ix).segment(1, ny_in) =
          algebra::tridiagonal(std::move(m), std::move(r));
    }

    u = u_trans.transpose();
  }

 private:
  static const complex compute_a(const complex &A, const real dz,
                                 const real dt) {
    return A * dz / (dt * dt);
  }

  static const array_2D compute_f(const Eigen::Ref<const array_2D> &F,
                                  const real dz) {
    return F * dz;
  }
};

}  // namespace finite_differences
