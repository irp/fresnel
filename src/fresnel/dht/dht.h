#pragma once

#include <Eigen/Dense>
#include <algorithm>
#include <cmath>

namespace hankel {

template <typename T>
using array1d = Eigen::Array<T, Eigen::Dynamic, 1>;

template <typename T>
array1d<T> dht(const Eigen::Ref<const array1d<T>> in, const unsigned order,
               const Eigen::Ref<const array1d<double>> jn) {
  const int n = std::min(in.size(), jn.size() - 1);
  const double jn_n = jn[n];
  auto jn_v = jn.segment(0, n);

  array1d<double> bessel_o1 = jn_v.unaryExpr(
      [order](double x) { return std::cyl_bessel_j(order + 1, x); });
  array1d<T> in_mult = 2.0 / jn_n * bessel_o1.square().inverse() * in;

  array1d<T> out(n, 1);
  for (int i = 0; i < n; ++i) {
    array1d<double> row(n);
#pragma omp parallel for
    for (int j = 0; j < n; ++j) {
      row[j] = std::cyl_bessel_j(order, jn_v[i] * jn_v[j] / jn_n);
    }
    out[i] = row.matrix().dot(in_mult.matrix());
  }

  return out;
}
}  // namespace hankel
