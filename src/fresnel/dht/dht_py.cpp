#include <nanobind/nanobind.h>
#include <nanobind/eigen/dense.h>
#include <nanobind/stl/complex.h>

#include <complex>

#include "./dht.h"

namespace nb = nanobind;

NB_MODULE(_dht, m) {
  m.def("dht", &hankel::dht<std::complex<double>>, nb::arg("in").noconvert(),
        nb::arg("order").noconvert(), nb::arg("jn").noconvert(),
        nb::rv_policy::move);
}
