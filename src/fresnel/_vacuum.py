# SPDX-License-Identifier: GPL-3.0-or-later

import numpy as np
from scipy.fft import fftn, ifftn, fftshift, ifftshift, fftfreq
from scipy.signal import ZoomFFT

from . import hankel
from ._misc import fftfreqn, gridn, crop, squaresum


def _verify_fresnel_numbers_cs(fresnel_numbers):
    fresnel_numbers = np.atleast_1d(fresnel_numbers)

    if fresnel_numbers.ndim > 1:
        raise ValueError("Parameter `fresnel_numbers` must be either scalar or 1-dimensional.")

    return fresnel_numbers


def _verify_fresnel_numbers(fresnel_numbers, ndim):
    fresnel_numbers = np.asarray(fresnel_numbers)
    if np.ndim(fresnel_numbers) == 0:
        fresnel_numbers = fresnel_numbers * np.ones((1, ndim))
    elif np.ndim(fresnel_numbers) == 1:
        fresnel_numbers = fresnel_numbers[:, np.newaxis] * np.ones((1, ndim))
    elif np.ndim(fresnel_numbers) == 2:
        if fresnel_numbers.shape[1] != ndim:
            raise ValueError("Parameter `fresnel_numbers` incompatible with data dimension.")
        # fresnel_numbers has the correct shape
    else:
        raise ValueError("Parameter `fresnel_numbers` must either be scalar, 1d, or 2d array.")

    return fresnel_numbers


def calculate_fresnel_numbers(dperp, dz, wavelength, n=1):
    # TODO document
    dperp = np.asarray(dperp)
    dz = np.atleast_1d(dz)

    return (dperp * n) ** 2 / (dz * wavelength)


class FresnelPropagatorCS:
    r"""Fresnel propagator for circular-symmetric fields.

    Using the method outlined in :footcite:p:`GuizarSicairos_JotOSoAA_2004`.

    Parameters
    ----------
    nsamples : int
        The number of sampling points.
    fresnel_numbers : array
        List of (pixel) Fresnel numbers corresponding to different propagation distances.


    Notes
    -----
    TODO: describe Fresnel numbers

    If ``ndist = fresnel_numbers.shape[0]``, the number of propagation distances, is greater than one, ``f(u)`` will
    return a 2-dimensional array of dimension ``ndist`` x ``nsamples``. Otherwise it will return a 1-dimensional array.

    References
    ----------
    .. footbibliography::
    """

    def __init__(self, nsamples, fresnel_numbers, hankel_order=0):
        self._nsamples = nsamples
        self._order = hankel_order

        self.fresnel_numbers = _verify_fresnel_numbers_cs(fresnel_numbers)
        self._ndist = self.fresnel_numbers.shape[0]

        self.kernel = self._init_kernel()
        self._dht = hankel.DiscreteHankelTransform(self._nsamples, self._order)

    @staticmethod
    def calculate_fresnel_numbers(nsamples, xmax, dz, wavelength):
        r"""
        Calculate Fresnel number for the given parameters.

        Parameters
        ----------
        nsamples : int
           number of samples
        xmax : float
            Maximum radius
        dz : float
            Propagation distance
        wavelength : float
            Wave length of the radiation

        Returns
        -------
        out : ndarray
            Fresnel numbers.
        """
        return (2 * xmax / nsamples) ** 2 / (dz * wavelength)

    def _init_kernel(self):
        # convert from pixel-based to support-based fresnel numbers
        _fres = self.fresnel_numbers * self._nsamples**2
        # the factor of (1/2)^2 from diam = 2 * radius is inserted in the kernel

        freq = hankel.hankel_freq(self._nsamples, xmax=1)
        kern = np.exp(-1j * np.pi / (0.25 * _fres[:, np.newaxis]) * np.square(freq)[np.newaxis, :])

        return kern

    def grid(self, xmax):
        r"""
        Sampling grid.

        Parameters
        ----------
        xmax : float
            Maximum radius

        Returns
        -------
        out : ndarray
            The sampling grid.
        """
        return xmax * hankel.hankel_samples(N=self._nsamples, xmax=1, order=self._order)

    def interpolate(self, y, x_new, xmax):
        r"""
        Interpolate wave field for given sampling points.

        Parameters
        ----------
        y : complex
            Fields evaluated at implicit sampling points.
        x_new : float
            New sampling points.
        xmax : float
            Maximum radius

        Returns
        -------
        out : ndarray
            The sampling grid.
        See Also
        --------
        FresnelPropagatorCS.grid
        """
        interp = hankel.HankelInterpolate(x_new, self._nsamples, xmax, order=self._order)
        return interp(y)

    def __call__(self, u):
        r"""Propagate a signal.

        Parameters
        ----------
        u : array
            The wavefield to propagate.

        Returns
        -------
        out : ndarray
            The propagated wavefield.
        """
        u = np.asarray(u)

        if np.ndim(u) != 1 or u.shape[0] != self._nsamples:
            raise ValueError("Invalid shape.")

        hf_u = self._dht(u)
        hf_uprop = self.kernel * hf_u[np.newaxis, :]
        uprop = np.zeros_like(u, shape=(self._ndist, *u.shape))

        for dist in range(self._ndist):
            uprop[dist, :] = self._dht(hf_uprop[dist, :])

        if self._ndist == 1:
            return uprop[0, :]
        else:
            return uprop


class FTConvolutionPropagator:
    r"""Base class for creating callable Fourier-Transform convolution propagators.

    This class is not intended to be used directly and does nothing.

    Parameters
    ----------
    shape : tuple
        Shape of input wavefields.
    npad : array, optional
        Padding factor.
    """

    def __init__(self, shape, npad):
        self._shape = tuple(shape)
        self._ndim = len(self._shape)
        self._npad = np.asarray(npad) * np.ones((self._ndim,))

        if np.any(self._npad < 1):
            raise ValueError("Padding factor `npad` cannot be less than 1.")

        # calculate amount of padding
        _pad_before = (np.asarray(self._shape) * (self._npad - 1) / 2).astype(int)
        _pad_after = _pad_before
        self._shape_pad = self._shape + _pad_before + _pad_after
        self._pad_width = tuple(zip(tuple(_pad_before), tuple(_pad_after)))

        self.kernel = 1
        self._ndist = 1

    def __call__(self, u, workers=-1):
        r"""Propagate a signal.

        Parameters
        ----------
        u : array
            The input wavefield.

        Returns
        -------
        out : ndarray
            The propagated wavefield.
        """
        u = np.asarray(u)

        if u.shape != self._shape:
            raise ValueError("Invalid shape.")

        # pad with zeros
        upad = np.pad(u, self._pad_width)

        # compute fft over last ndim axes
        axes = list(range(-self._ndim, 0))

        ft_u = fftn(upad, axes=axes, workers=workers)
        ft_uprop = ft_u * self.kernel
        uproppad = ifftn(ft_uprop, axes=axes, workers=workers)

        # crop central part
        pad_width_full = [
            (0, 0),
        ] + list(self._pad_width)
        uprop = crop(uproppad, pad_width_full)

        if self._ndist == 1:
            return uprop[0, :]
        else:
            return uprop


class FresnelTFPropagator(FTConvolutionPropagator):
    r"""Fresnel propagation using the transfer-function method.

    Refer to :footcite:p:`Goodman__2005` and :footcite:p:`Zhang_JotOSoAA_2020` for details.

    Parameters
    ----------
    shape : tuple
        Shape of input wavefields.
    fresnel_numbers: array-like
        List of Fresnel numbers corresponding to different propagation distances.
    npad : array, optional
        Padding factor.

    Notes
    -----
    If ``ndist = fresnel_numbers.shape[0]``, the number of propagation distances, is greater than one, ``f(u)`` will
    return a 2-dimensional array of dimension ``ndist`` x ``shape``. Otherwise the output will match the input shape.

    See Also
    --------
    FresnelIRPropagator : Fres-IR propagator

    References
    ----------
    .. footbibliography::
    """

    def __init__(self, shape, fresnel_numbers, npad=2):
        super(FresnelTFPropagator, self).__init__(shape, npad)

        self.fresnel_numbers = _verify_fresnel_numbers(fresnel_numbers, self._ndim)
        self._ndist = self.fresnel_numbers.shape[0]

        self.kernel = self._init_kernel()

    def _init_kernel(self):
        f = fftfreqn(self._shape_pad)

        kernel = np.ones((self._ndist, *self._shape_pad), dtype=np.complex128)
        for dist in range(self._ndist):
            for dim in range(self._ndim):
                kernel[dist, :] *= np.exp(
                    -1j * np.pi / self.fresnel_numbers[dist, dim] * np.square(f[dim])
                )

        return kernel


class FresnelIRPropagator(FTConvolutionPropagator):
    r"""Fresnel propagation using the impulse-reponse method.

    Refer to :footcite:p:`Goodman__2005` and :footcite:p:`Zhang_JotOSoAA_2020` for details.

    Parameters
    ----------
    shape : tuple
        Shape of input wavefields.
    fresnel_numbers: array-like
        List of Fresnel numbers corresponding to different propagation distances.
    npad : array, default=2
        Padding factor.

    Notes
    -----
    If ``ndist = fresnel_numbers.shape[0]``, the number of propagation distances, is greater than one, ``f(u)`` will
    return a 2-dimensional array of dimension ``ndist`` x ``shape``. Otherwise the output will match the input shape.

    See Also
    --------
    FresnelTFPropagator : Fres-TF propagator

    References
    ----------
    .. footbibliography::
    """

    def __init__(self, shape, fresnel_numbers, npad=2):
        super(FresnelIRPropagator, self).__init__(shape, npad)

        self.fresnel_numbers = _verify_fresnel_numbers(fresnel_numbers, self._ndim)
        self._ndist = self.fresnel_numbers.shape[0]

        self.kernel = self._init_kernel()

    def _init_kernel(self):
        # 1. assemble real-space convolution kernel (impulse response)
        conv_kernel = np.ones((self._ndist, *self._shape_pad), dtype=np.complex128)

        x = gridn(self._shape_pad)

        for dist in range(self._ndist):
            # scaling and phase factor
            conv_kernel[dist, ...] *= np.prod(
                np.sqrt(np.abs(self.fresnel_numbers[dist, :]))
            ) * np.prod(np.exp((-1j * np.pi / 4) * np.sign(self.fresnel_numbers[dist, :])))

            # chirp function
            for dim in range(self._ndim):
                conv_kernel[dist, ...] *= np.exp(
                    1j * np.pi * self.fresnel_numbers[dist, dim] * np.square(x[dim])
                )

        # 2. compute transfer function from convolution kernel
        axes = list(range(-self._ndim, 0))
        kernel = fftn(conv_kernel, axes=axes)

        return kernel


class ASMPropagator(FTConvolutionPropagator):
    """
    Propagation using the Angular Spectrum Method (ASM).

    Goodman calls this the "Exact Transfer Function approach".
    Voelz and Roggeman call this the "Rayleigh-Sommerfeld Transfer Function solution".
    Refer to :footcite:p:`Goodman__2005` and :footcite:p:`Zhang_JotOSoAA_2020` for details.

    Parameters
    ----------
    shape : tuple
        Shape of input wavefields.
    dperp : tuple
        Lateral pixel sizes.
    dz : array
        Propagation distance(s).
    wl : float, optional
        Wavelength in units of pixels.
    npad : array, optional
        Padding factor.
    mask_evanescent : bool, optional
        Mask evanescent spectral components.

    Notes
    -----
    Parameter ``dz`` must be either a scalar or 1d array. If ``ndist = len(dz)``, the number of propagation distances,
    is greater than one, ``f(u)`` will return a 2-dimensional array of dimension ``ndist`` x ``shape``. Otherwise
    the output will match the input shape.

    See Also
    --------
    FresnelTFPropagator : Fres-TF propagator
    FresnelIRPropagator : Fres-IR propagator


    References
    ----------
    .. footbibliography::
    """

    _lambda0 = 1.0  # all lengths are in units of the wavelength
    _k0 = 2 * np.pi / _lambda0

    def __init__(self, shape, dperp, dz, wl=1, npad=2, mask_evanescent=True):
        super(ASMPropagator, self).__init__(shape, npad)

        self._dperp = dperp  # pixel size
        self._wl = wl  # relative wavelength
        self.mask_evanescent = mask_evanescent

        dz = np.asarray(dz)
        if np.ndim(dz) == 0:
            self._dz = dz * np.ones((1,))
        elif np.ndim(dz) == 1:
            self._dz = dz
        else:
            raise ValueError("Parameter `dz` must either be scalar or 1d array.")

        self._ndist = self._dz.shape[0]

        self.kernel = self._init_kernel()

    def _init_kernel(self):
        f = fftfreqn(self._shape_pad, self._dperp)  # spatial frequencies
        f2 = squaresum(f)
        phase_chirp = np.sqrt(1 / np.square(self._wl) - f2.astype(np.complex128))

        mask = f2 < 1 / np.square(self._wl) if self.mask_evanescent else 1

        # expand dimension of dz
        dz = self._dz.reshape([-1] + self._ndim * [1])

        kernel = mask * np.exp(1j * self._k0 * dz * phase_chirp[np.newaxis, ...])

        return kernel


class FraunhoferPropagator:
    """
    Fraunhofer (far field) propagation in arbitrary dimensions.

    Parameters
    ----------
    shape : tuple
        Shape of input wavefields.
    dperp : tuple
        Lateral pixel sizes.
    dz : array
        Propagation distance(s).
    wl : float, optional
        Wavelength in units of pixels.

    Notes
    -----
    Parameter ``dz`` must be either a scalar or 1d array. If ``ndist = len(dz)``, the number of propagation distances,
    is greater than one, ``f(u)`` will return a 2-dimensional array of dimension ``ndist`` x ``shape``. Otherwise
    the output will match the input shape.
    """

    def __init__(self, shape, dperp, dz=1, wl=1):
        self._shape = tuple(shape)
        self._ndim = len(self._shape)
        self._dperp = np.asarray(dperp)

        dz = np.asarray(dz)
        if np.ndim(dz) == 0:
            self._dz = dz * np.ones((1,))
        elif np.ndim(dz) == 1:
            self._dz = dz
        else:
            raise ValueError("Parameter `dz` must either be scalar or 1d array.")

        self._ndist = self._dz.shape[0]

        self._wl = wl

        prefactor = (-1j / (self._wl * self._dz)) ** (self._ndim / 2) * np.prod(self._dperp)
        self._prefactor = prefactor.reshape([-1] + self._ndim * [1])

    def __call__(self, u, workers=-1):
        r"""Propagate a signal.

        Parameters
        ----------
        u : array
            The input wavefield.

        Returns
        -------
        out : ndarray
            The far field.
        workers : int, default=-1
            Number of worker threads used in the FFT.
        """
        u = np.asarray(u)

        # compute fft over last ndim axes
        axes = list(range(-self._ndim, 0))

        u_prop = (
            self._prefactor
            * ifftshift(
                fftn(fftshift(u, axes=axes), norm="backward", axes=axes, workers=workers), axes=axes
            )[np.newaxis, ...]
        )

        if self._ndist == 1:
            return u_prop[0]
        else:
            return u_prop

    def inverse(self, u_prop, workers=-1):
        r"""Propagate a signal.

        Parameters
        ----------
        u_prop : array
            The far field.

        Returns
        -------
        out : ndarray
            The propagated wavefield.
        workers : int, default=-1
            Number of worker threads used in the FFT.
        """
        u_prop = np.asarray(u_prop)

        # compute fft over last ndim axes
        axes = list(range(-self._ndim, 0))

        u = fftshift(
            ifftn(
                ifftshift(u_prop / self._prefactor, axes=axes),
                norm="backward",
                axes=axes,
                workers=workers,
            ),
            axes=axes,
        )

        if self._ndist == 1:
            return u[0]
        else:
            return u

    def coordinates(self, retstep=False):
        r"""Far-field coordinates.

        Parameters
        ----------
        retstep : bool, default=False
            Return the distance between two adjacent coordinates

        Returns
        -------
        theta : ndarray
            The far-field angles for the sampling parameters of this propagator.
        """
        theta = [fftshift(fftfreq(n, d / self._wl)) for n, d in zip(self._shape, self._dperp)]
        dtheta = [(self._wl / (d * n)) for n, d in zip(self._shape, self._dperp)]

        if not retstep:
            return theta
        else:
            return theta, dtheta

    def field_of_view(self):
        r"""Far-field coordinates.

        Returns
        -------
        fov : [(thi_min, thi_max), ...]
            The minimum and maximum far-field angles for the sampling parameters of this propagator.
        """
        return [(x.min(), x.max()) for x in self.coordinates()]


class FraunhoferPropagator1dZoom:
    """
    Create a callable Fraunhofer (far field) propagator.

    PROTOTYPE
    """

    ndim = 1

    def __init__(
        self,
        shape_in,
        dx,
        shape_out,
        out_min,
        out_max,
        *,
        in_min=None,
        distance=1.0,
        wavelength=1.0,
        endpoint=False,
    ):
        self._dx = dx / wavelength

        range_out = (out_min / distance, out_max / distance)

        if not in_min:
            in_min = -dx * (shape_in - 1) / 2

        in_min = in_min / wavelength
        self._theta = np.linspace(*range_out, shape_out, endpoint=endpoint)

        self._zoomfft = ZoomFFT(
            shape_in, fn=range_out, m=shape_out, fs=1 / self._dx, endpoint=endpoint
        )

        chirp_offset = np.exp(-2j * np.pi * self._theta * in_min)
        distance_wl = distance / wavelength
        chirp_global = (1.0j * distance_wl) ** (-self.ndim / 2)
        chirp_oscillating = 1  # np.exp(1j * np.pi * distance_wl * self._theta**2)

        self._chirp = chirp_offset * chirp_global * chirp_oscillating

    def __call__(self, u, axis=-1):
        u = np.asarray(u)
        return self._chirp * self._dx * self._zoomfft(u, axis=axis)

    @property
    def coordinates(self):
        return self._theta

    @property
    def nyquist(self):
        return 1 / (2 * self._dx)
