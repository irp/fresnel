# SPDX-License-Identifier: GPL-3.0-or-later
r"""
============================
Fresnel API (:mod:`fresnel`)
============================

.. currentmodule:: fresnel


Finite-Difference and Multislice Propagation
============================================

.. autosummary::
    :toctree: generated/

    FDPropagator2d
    FDPropagator3d
    FresnelPropagatorCS
    MultislicePropagator


Free-Space Propagation
======================

.. autosummary::
    :toctree: generated/

    FresnelIRPropagator
    FresnelTFPropagator
    FresnelPropagatorCS
    ASMPropagator
    FraunhoferPropagator


Analytic Solutions
==================

.. autosummary::
    :toctree: generated/

    DiskAperture
    RectangularAperture
    GaussianAperture
    gaussian_beam1d

"""

from ._propagate import FDPropagator2d, FDPropagator3d, FDPropagatorCS, MultislicePropagator
from ._vacuum import (
    FresnelIRPropagator,
    FresnelTFPropagator,
    FresnelPropagatorCS,
    ASMPropagator,
    FraunhoferPropagator,
    calculate_fresnel_numbers,
)

from ._analytic import DiskAperture, RectangularAperture, GaussianAperture, gaussian_beam1d

__all__ = [
    "FresnelIRPropagator",
    "FresnelTFPropagator",
    "FresnelPropagatorCS",
    "ASMPropagator",
    "FraunhoferPropagator",
    "calculate_fresnel_numbers",
    "FDPropagator2d",
    "FDPropagator3d",
    "FDPropagatorCS",
    "MultislicePropagator",
    "DiskAperture",
    "RectangularAperture",
    "GaussianAperture",
    "gaussian_beam1d",
]
