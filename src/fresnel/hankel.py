# SPDX-License-Identifier: GPL-3.0-or-later

import numpy as np
from scipy.special import jv as bessel_jv, jn_zeros

from ._dht import dht


def hankel_samples(N, xmax=1, order=0):
    r"""
    Real space sampling grid for the DHT

    Parameters
    ----------
    N : int
        number of pixels
    xmax : float, optional
        width of real space grid
    order : int, optional
        order of DHT


    Returns
    -------
    array
        real space sampling grid
    """

    an = jn_zeros(order, N + 1)

    return xmax * an[:-1] / an[N]


def hankel_freq(N, xmax=1, order=0):
    r"""
    Hankel space (frequency) sampling grid for the inverse DHT

    Parameters
    ----------
    N : int
        number of pixels
    xmax : float, optional
        width of real space grid
    order : int, optional
        order of DHT


    Notes
    -----
    The frequency scale is `scipy.special.jn_zeros(order, N +1) / xmax`.


    Returns
    -------
    array
        frequency sampling grid
    """

    an = jn_zeros(order, N)

    return an / (2 * np.pi * xmax)


class HankelInterpolate:
    r"""
    Interpolate a function sampled on the sampling grid of the DHT as in [1]_.

    Parameters
    ----------
    N : int
        number of samples
    xmax : float
        radius of the DHT sampling grid
    order : int
        order of the DHT
    tol : float
        tolerance

    See also
    --------
    fresnel.DiscreteHankelTransform

    References
    ----------
    .. [1] A. W. Norfolk and E. J. Grace, “Reconstruction of optical fields with the Quasi-discrete Hankel transform,”
           Optics Express, vol. 18, no. 10, p. 10551, May 2010.
           https://doi.org/10.1364regularization of the resampling%2Foe.18.010551
    """

    def __init__(self, x_new, N, xmax=1, order=0, matrix_free=True, tol=1e-10):
        self._tol = tol

        # work with dimensionless lengths, so that xmax = 1
        self.x_old = hankel_samples(N, xmax=1, order=order)
        self.x_new = x_new / xmax

        self.n_new = self.x_new.shape[-1]
        self.n_old = self.x_old.shape[-1]

        aN = jn_zeros(order, N + 1)[N]

        self._order = order
        self._coeff = 2 * self.x_old / aN / bessel_jv(order + 1, aN * self.x_old)
        self._factor = bessel_jv(self._order, aN * self.x_new)

        if not matrix_free:
            self._mat = self.matrix()
        else:
            self._mat = None

    def matrix(self):
        mat = np.zeros((self.n_new, self.n_old))

        for i in np.arange(self.n_old):
            mat[i, :] = self._get_row(i)

        return mat

    def _get_row(self, i):
        diff = self.x_old**2 - self.x_new[i] ** 2
        min_idx = np.argmin(np.abs(diff))

        if np.abs(diff[min_idx]) <= self._tol:
            # use nearest neighbour for distance smaller than tolerance
            f = np.zeros_like(self._coeff)
            f[min_idx] = 1
        else:
            f = self._factor[i] * self._coeff / diff

        return f

    def __call__(self, y):
        if self._mat is not None:
            # transposed, because y may have additional dimensions
            y_new = y * self._mat.T
        else:
            new_shape = (*y.shape[:-1], self.n_new)
            y_new = np.empty(shape=new_shape, dtype=y.dtype)

            for i in np.arange(self.n_new):
                f = self._get_row(i)
                y_new[..., i] = np.dot(y, f)

        return y_new


class DiscreteHankelTransform:
    r"""
    Discrete Hankel transform (DHT) [1]_ of order `order`.

    Parameters
    ----------
    N : int
        number of pixels
    order : int, optional
        order of DHT

    Notes
    -----
    The Hankel matrix is self-inverse, i.e.

    .. math:: YY = \mathbb{1}.

    References
    ----------
    .. [1] N. Baddour and U. Chouinard, “Theory and operational rules for the discrete Hankel transform,”
           Journal of the Optical Society of America A, vol. 32, no. 4, p. 611, Mar. 2015.
           https://doi.org/10.1364/JOSAA.32.000611
    """

    def __init__(self, N, order=0, matrix_free=True):
        self._N = N
        self._order = order
        self._an = jn_zeros(order, N + 1)
        self._order = order

        if not matrix_free:
            self._mat = self.matrix()
        else:
            self._mat = None

    def __call__(self, x):
        if self._mat is None:
            # use matrix-free C++ implementation
            return dht(x, self._order, self._an)
        else:
            return self._mat @ x

    def matrix(self):
        aN = self._an[-1]

        m, k = np.ogrid[0 : self._N, 0 : self._N]

        mat = (
            bessel_jv(self._order, self._an[m] * self._an[k] / aN)
            * 2
            / (aN * bessel_jv(self._order + 1, self._an[k]) ** 2)
        )

        return mat
