*************
API Reference
*************

.. automodule:: fresnel
   :no-members:
   :no-inherited-members:
   :no-special-members: