:html_theme.sidebar_secondary.remove: true

.. py::currentmodule:: fresnel

*********************
Fresnel documentation
*********************

**Date**: |today| **Version**: |version|

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   reference/index
