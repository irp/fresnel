# Fresnel

Wave propagation in free space and in waveguides.

Implemented methods:
 - Fresnel propagation in free space
 - Fraunhofer propagation in free space
 - Finite-difference propagation for 1+1d, 2+1d, and cylidrical geometry (as compiled C++ extensions)
 - Analytic solutions for Gaussian and rectangular apertures

## Installation

### From wheel / binary
We provide compiled wheels for Linux that are compatible with a wide range of distributions (manylinux).
They can be installed via
```
pip install fresnel --index-url https://gitlab.gwdg.de/api/v4/projects/14428/packages/pypi/simple
```


### Conda
We compile conda builds but do not upload them to a registry at the moment.

### From source
The project can be readily installed from source.
Simply run
```
git clone git@gitlab.gwdg.de:irp/fresnel.git fresnel-git
cd fresnel-git
pip install .
```
This builds and installs the project including the C++ extensions. Under the hood, it uses `meson` and `meson-python` (via PEP517). 
This **requires a recent C++ compiler**.
The extensions depend on 
  - `eigen` 
  - `nanobind`

If the dependencies are not available on the system, they are automatically fetched through mesons [Wrap dependency system](https://mesonbuild.com/Wrap-dependency-system-manual.html).

## Documentation

See the sphinx-generated documentation, here https://irp.pages.gwdg.de/fresnel/.
Also have a look at the examples.

## Attribution
If you use the library in a scientific project, please cite our paper:
```
@Article{Soltau_OE_2021,
  author    = {Soltau, Jakob and Lohse, Leon Merten and Osterhoff, Markus and Salditt, Tim},
  journal   = {Optics Express},
  title     = {Finite-difference propagation for the simulation of x-ray multilayer optics},
  year      = {2021},
  month     = dec,
  number    = {25},
  pages     = {41932},
  volume    = {29},
  doi       = {10.1364/oe.445300},
  publisher = {Optica Publishing Group},
}

```
